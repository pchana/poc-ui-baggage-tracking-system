<!doctype html>
<html lang="en">

<head>
    <?php include 'component/header.php'; ?>
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="asset/logo-airplane.png" width="30" height="30" class="d-inline-block align-top" alt="">
            Airplane
        </a>
    </nav>
    <div class="container">
        <div class="py-5 text-center">
            <img class="d-block mx-auto mb-4" src="asset/logo-airplane.png" alt="" width="72" height="72">
            <h2>รับกระเป๋าเข้าสู่ระบบ</h2>
            <!-- <p class="lead">Below is an example form built entirely with Bootstrap's form controls. Each required form
                group has a validation state that can be triggered by attempting to submit the form without completing
                it.</p> -->
        </div>

        <div class="row">
            <div class="col-md-8 order-md-1">
                <h4 class="mb-3">รายละเอียด</h4>
                <form class="needs-validation" novalidate>
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="flight_number">หมายเลขรหัสเที่ยวบิน</label>
                            <input type="text" class="form-control" id="flight_number" placeholder="" value="" required>
                        </div>
                        <div class="col-md mb-3">
                            <label for="bag_tag">หมายเลขกระเป๋า</label>
                            <input type="text" class="form-control" id="bag_tag" placeholder="" value="" required>
                        </div>
                    </div>
                    <!-- <h4 class="mb-3">Payment</h4> -->
                    <div class="d-block my-3">
                        <div class="custom-control custom-radio">
                            <input id="type1" value="type1" name="type_bag" type="radio" class="custom-control-input" checked
                                required>
                            <label class="custom-control-label" for="type1">ประเภท1</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="type2" value="type2" name="type_bag" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label" for="type2">ประเภท2</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="type3" value="type3" name="type_bag" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label" for="type3">ประเภท3</label>
                        </div>
                        <div class="custom-control custom-radio">
                            <input id="type4" name="type_bag" type="radio" class="custom-control-input" required>
                            <label class="custom-control-label mr-1" for="type4">อื่นๆ</label>
                            <input id="type4_text" type="text" class="form-control">
                        </div>
                    </div>
                </form>
            </div>
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">รายละเอียด</span>
                    <!-- <span class="badge badge-secondary badge-pill">3</span> -->
                </h4>
                <ul class="list-group mb-3">
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">หมายเลขรหัสเที่ยวบิน</h6>
                        </div>
                        <span class="text-muted" id="flight_number_txt"></span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">หมายเลขกระเป๋า</h6>
                        </div>
                        <span class="text-muted" id="bag_tag_txt"></span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">ประเภทกระเป๋า</h6>
                        </div>
                        <span class="text-muted" id="bag_type_txt"></span>
                    </li>
                    <li class="list-group-item d-flex justify-content-between lh-condensed">
                        <div>
                            <h6 class="my-0">วันที่และเวลา</h6>
                        </div>
                        <span class="text-muted" id="datetime"></span>
                    </li>
                    <!-- <li class="list-group-item d-flex justify-content-between">
                        <span>Check-In User</span>
                        <strong>Chana Mahat</strong>
                    </li> -->
                </ul>

                <button class="btn btn-primary btn-lg btn-block" type="submit">ยืนยัน</button>
            </div>
        </div>

        <footer class="my-5 pt-5 text-muted text-center text-small">
            <!-- <p class="mb-1">&copy; 2017-2018 Company Name</p>
            <ul class="list-inline">
                <li class="list-inline-item"><a href="#">Privacy</a></li>
                <li class="list-inline-item"><a href="#">Terms</a></li>
                <li class="list-inline-item"><a href="#">Support</a></li>
            </ul> -->
        </footer>
    </div>

    <!-- Bootstrap JS -->
    <script src="js/jquery/jquery.min.js"></script>
    <script>
    //set date time
    var dateTime = new Date();
    document.getElementById("datetime").innerHTML = dateTime.toLocaleString();

    //radio
    $("#type4_text").hide();
    $("#bag_type_txt").text($("input[type=radio][name=type_bag]").val());
    $("input[type=radio][name=type_bag]").change(function() {
        if ($("#type4").is(":checked")) {
            $("#type4_text").show(300);
        } else {
            $("#type4_text").hide(300);
            $("#type4_text").val("");
            $("#bag_type_txt").text($("input[name='type_bag']:checked").val());
        }
    });
    $("#type4_text").on('change keydown paste input', function() {
        $("#bag_type_txt").text($("#type4_text").val());
    });

    $("#flight_number").on('change keydown paste input', function() {
        $("#flight_number_txt").text($("#flight_number").val());
    });
    $("#bag_tag").on('change keydown paste input', function() {
        $("#bag_tag_txt").text($("#bag_tag").val());
    });
    </script>
</body>

</html>