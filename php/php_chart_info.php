<?php
    include('config/database.php');
    date_default_timezone_set('Asia/Bangkok');
    //Y-m-d H:i:s
    $end = date("Y-m-d H:i:s");
    $start = date("Y-m-01 00:00:00", strtotime( $end."- 11 month"));
    $data = array_fill(0,12,0);

    $sql = "SELECT * FROM sell_order WHERE created_time BETWEEN '$start' AND '$end'";
    $result = mysqli_query($conn,$sql);
    while(($row = mysqli_fetch_assoc($result))) {
        $data[(int)(date('m',strtotime($row['created_time']))-1)]++;
    }
    
    $array_month = json_decode(file_get_contents("../asset/json/month.json"), true);
    
    $i = 0;
    $start_month = (int)(date('m',strtotime($start)));
    $month = array_fill(0,12,0);
    while($i < 12){
        $month[$i] = $array_month[$start_month];
        if($i < (int)(date('m',strtotime($start)))-1){
            array_push($data,array_shift($data));
        }
        $start_month++;
        if($start_month > 12){
            $start_month = 1;
        }
        $i++;
        
    }
    $response['data'] = $data;
    $response['month'] = $month;

    echo json_encode($response);
?>