<?php
    include('config/database.php');
    date_default_timezone_set('Asia/Bangkok');
    //Y-m-d H:i:s

    $month = '06';

    $start = date("Y-$month-01 00:00:00");
    $end = date("Y-m-d H:i:s",strtotime( date("Y-m-01 23:59:59", strtotime( $start."+ 1 month"))."- 1 day"));
    $response = array_fill(0, date('d',strtotime($end)),0);

    $sql = "SELECT * FROM sell_order WHERE created_time BETWEEN '$start' AND '$end'";
    $result = mysqli_query($conn,$sql);
    while(($row = mysqli_fetch_assoc($result))) {
        $response[(int)(date('d',strtotime($row['created_time']))-1)]++;
    }
    // if(isset($_POST['username'])
    echo json_encode($response);
?>