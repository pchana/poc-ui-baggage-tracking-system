<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <?php include 'component/header.php'; ?>

    <link href="plugin/sb-admin/css/sb-admin-2.min.css" rel="stylesheet">
    <link href="plugin/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link href="plugin/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
    <title>Airplane</title>
</head>

<body>
    <nav class="navbar navbar-light bg-light">
        <a class="navbar-brand" href="#">
            <img src="asset/logo-airplane.png" width="30" height="30" class="d-inline-block align-top" alt="">
            Airplane
        </a>
    </nav>

    <div class="row">
        <div class="col-xl-3 col-md-6 mb-4 mt-3 ml-4">
            <div class="card border-left-info shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Flight Number
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">125122</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-plane fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-xl-3 col-md-6 mb-4 mt-3 ml-4">
            <div class="card border-left-primary shadow h-100 py-2">
                <div class="card-body">
                    <div class="row no-gutters align-items-center">
                        <div class="col mr-2">
                            <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">All Bag Tag
                            </div>
                            <div class="h5 mb-0 font-weight-bold text-gray-800">15412145</div>
                        </div>
                        <div class="col-auto">
                            <i class="fas fa-calendar fa-2x text-gray-300"></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid mb-4 mt-3">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <!-- Bar Chart Month12 -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <h6 class="m-0 font-weight-bold text-primary">Bar Chart</h6>
                            <select class="custom-select custom-select-sm col-2 ml-auto" id="barchart-month-select">
                                <option value="year" selected>เดือน</option>
                            </select>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="barChart_month"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <!-- Bar Chart day -->
                <div class="card shadow mb-4">
                    <div class="card-header py-3">
                        <div class="row">
                            <h6 class="m-0 font-weight-bold text-primary">Bar Chart</h6>
                            <select class="custom-select custom-select-sm col-3 ml-auto" id="barchart-day-select">
                                <?php
                                    $array_month = json_decode(file_get_contents("asset/json/month.json"), true);
                                    for($i=12 ; $i > (int)(date('m',strtotime(date("Y-m-d H:i:s")))) ; $i--){
                                        unset($array_month[$i]);
                                    }
                                    foreach($array_month as $key=>$month){
                                        echo "<option value=\"$key\">$month</option>";
                                    }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="chart-bar">
                            <canvas id="barChart_month"></canvas>
                        </div>
                        <hr>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- DataTales Example -->
    <div class="container-fluid">
        <!-- https://gijgo.com/datepicker/example/localization -->
        <div class="row">
            <div class="col-5 input-group mb-3">
                <input type="text" class="form-control" aria-label="Recipient's username"
                    aria-describedby="basic-addon2">
                <div class="input-group-append">
                    <button class="btn btn-outline-secondary" type="button">ค้นหา</button>
                </div>
            </div>
        </div>
        <div class="card shadow mb-4 block">
            <div class="card-header py-3">
                <div class="row">
                    <h6 class="m-0 font-weight-bold text-primary">ข้อมูล</h6>
                    <div class="dropdown ml-auto">
                        <button class="btn btn-secondary dropdown-toggle btn-sm" type="button" id="report_dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <!-- <i class="fa fa-file-text-o" aria-hidden="true"></i> -->
                            <i class="fas fa-file  fa-lg text-gray-300"></i>
                            รายงานข้อมูล
                        </button>
                        <div class="dropdown-menu" aria-labelledby="report_dropdown">
                            <a class="dropdown-item" href="#"><i
                                    class="fas fa-file-excel fa-2x text-gray-700 mr-2"></i>Excel</a>
                            <a class="dropdown-item" href="#"><i
                                    class="fas fa-file-pdf fa-2x text-gray-700 mr-2"></i>PDF</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                        <thead>
                            <tr>
                                <th>หมายเลขรหัสเที่ยวบิน</th>
                                <th>หมายเลขกระเป๋าสัมภาระ</th>
                                <th>ประเภทกระเป๋า</th>
                                <th>ผู้รับกระเป๋าเข้าระบบ</th>
                                <th>ผู้รับกระเป๋าออกระบบ</th>
                                <th>วันที่และเวลา</th>
                            </tr>
                        </thead>
                        <tfoot>
                            <tr>
                                <th>หมายเลขรหัสเที่ยวบิน</th>
                                <th>หมายเลขกระเป๋าสัมภาระ</th>
                                <th>ประเภทกระเป๋า</th>
                                <th>ผู้รับกระเป๋าเข้าระบบ</th>
                                <th>ผู้รับกระเป๋าออกระบบ</th>
                                <th>วันที่และเวลา</th>
                            </tr>
                        </tfoot>
                        <tbody>
                            <tr>
                                <td>TG-5123</td>
                                <td>0074KL774266070</td>
                                <td>ประเภท1</td>
                                <td>ชนา มหัท</td>
                                <td>นำออก แล้วนะ</td>
                                <td>2019/01/02</td>
                            </tr>
                            <tr>
                                <td>TG-1512</td>
                                <td>0074KL774266071</td>
                                <td>ประเภท3</td>
                                <td>นำเข้า สิครับ</td>
                                <td>นำออก แล้วนะ</td>
                                <td>2020/01/02</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <script src="js/jquery/jquery.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>

    <!-- Core plugin JavaScript-->
    <script src="js/jquery-easing/jquery.easing.min.js"></script>

    <!-- Page level plugins -->
    <script src="plugin/datatables/jquery.dataTables.min.js"></script>
    <script src="plugin/datatables/dataTables.bootstrap4.min.js"></script>

    <!-- Chart JS -->
    <script src="plugin/chart/chart.js/Chart.min.js"></script>

    <!-- Page level custom scripts -->
    <script src="plugin/chart/chart-bar-demo.js"></script>

    <script>
    $(document).ready(function() {
        $('#dataTable').DataTable({
            "language": {
                "url": "https://cdn.datatables.net/plug-ins/1.10.21/i18n/Thai.json"
            }
        });
        $('#dataTable').DataTable();
    });
    </script>
</body>

</html>